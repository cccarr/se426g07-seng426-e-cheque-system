/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eCheque;

import java.security.PrivateKey;
import java.security.PublicKey;
import junit.framework.TestCase;
import java.security.*;

/**
 *
 * @author cccarr
 */
public class DigitalsignetureTest extends TestCase {
    
//    KeyPairGenerator keyGen;
//    PrivateKey privKey;
//    PublicKey pubKey;
    public DigitalsignetureTest(String testName) throws Exception {
        super(testName);
    }

    /**
     * Test of signeture method, of class Digitalsigneture.
     */
    public void testSigneture() throws Exception {
        System.out.println("signeture");
        String message = "HELLO";
        Digitalsigneture instance = new Digitalsigneture();
      
        boolean verifyResult;
        byte[] result;
        
        /* Generate public/private key pair */
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        SecureRandom SecRandom = SecureRandom.getInstance("SHA1PRNG");
        keyGen.initialize(1024, SecRandom);
        KeyPair keypair = keyGen.generateKeyPair();
        PrivateKey privKey = keypair.getPrivate();
        PublicKey pubKey = keypair.getPublic();
        
        result = instance.signeture(message, privKey);
        
        /* test if sig is valid */
        Signature checkMessage = Signature.getInstance("SHA1withRSA");
        checkMessage.initVerify(pubKey);
        checkMessage.update(message.getBytes());
        verifyResult = checkMessage.verify(result);      
        assertTrue(verifyResult);
    }

    /**
     * Test of verifySignature method, of class Digitalsigneture.
     */
    public void testVerifySignature() throws Exception {
        System.out.println("verifySignature");
        String message = "HELLO";
        String falsemessage = "BOB";
        Digitalsigneture instance = new Digitalsigneture();
        SecureRandom SecRandom= new SecureRandom();
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024, SecRandom);
        KeyPair keypair = keyGen.genKeyPair();
        PrivateKey privKey  = keypair.getPrivate();
        PublicKey pubKey  = keypair.getPublic();
        
        Signature signmessage = Signature.getInstance("SHA1withRSA");
        signmessage.initSign(privKey);
        signmessage.update(message.getBytes());
        byte[]signature = signmessage.sign();
        
        /* Test valid */
        boolean result = instance.verifySignature(signature, message, pubKey);
        assertTrue(result);
		  
        /* Test bad message message */
        result = instance.verifySignature(signature, falsemessage, pubKey);
        assertFalse(result);
       
    }
    
}
