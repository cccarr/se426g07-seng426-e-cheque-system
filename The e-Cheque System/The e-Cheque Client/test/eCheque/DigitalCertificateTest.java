/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eCheque;

import java.security.PublicKey;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 *
 * @author mrivett
 */
public class DigitalCertificateTest extends TestCase {
    
    public DigitalCertificateTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(DigitalCertificateTest.class);
        return suite;
    }

    /**
     * Test of setHolderName method, of class DigitalCertificate.
     */
    public void testHolderName() {
        System.out.println("setHolderName");
        String x = "123";
        DigitalCertificate instance = new DigitalCertificate();
        instance.setHolderName(x);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(instance.getHolderName()==x);
    }

    /**
     * Test of setSubject method, of class DigitalCertificate.
     */
    public void testSubject() {
        System.out.println("setSubject");
        String x = "123";
        DigitalCertificate instance = new DigitalCertificate();
        instance.setSubject(x);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(instance.getSubject()==x);
    }

    /**
     * Test of setIssuer method, of class DigitalCertificate.
     */
    public void testIssuer() {
        System.out.println("setIssuer");
        String x = "123";
        DigitalCertificate instance = new DigitalCertificate();
        instance.setIssuer(x);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(instance.getIssuer()==x);
    }

    /**
     * Test of setSerialNumber method, of class DigitalCertificate.
     */
    public void testSerialNumber() {
        System.out.println("setSerialNumber");
        String x = "123";
        DigitalCertificate instance = new DigitalCertificate();
        instance.setSerialNumber(x);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(instance.getSerialNumber() == x);
    }

    /**
     * Test of setValidFrom method, of class DigitalCertificate.
     */
    public void testValidFrom() {
        System.out.println("setValidFrom");
        String x = "123";
        DigitalCertificate instance = new DigitalCertificate();
        instance.setValidFrom(x);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(instance.getValidFrom() == x);
    }

    /**
     * Test of setValidTo method, of class DigitalCertificate.
     */
    public void testValidTo() {
        System.out.println("setValidTo");
        String x = "123";
        DigitalCertificate instance = new DigitalCertificate();
        instance.setValidTo(x);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(instance.getValidTo() == x);
    }

    /**
     * Test of setPublicKey method, of class DigitalCertificate.
     */
    public void testPublicKey() {
        System.out.println("setPublicKey");
        PublicKey x = null;
        DigitalCertificate instance = new DigitalCertificate();
        instance.setPublicKey(x);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(instance.getpublicKey() == x);
    }

    /**
     * Test of setIssuerSignature method, of class DigitalCertificate.
     */
    public void testIssuerSignature() {
        System.out.println("setIssuerSignature");
        String s="e04fd020ea3a6910a2d808002b30309d";
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        byte[] x = data;
        DigitalCertificate instance = new DigitalCertificate();
        instance.setIssuerSignature(x);
        assertTrue(instance.getIssuerSignature() == x);
        
    }
    
}
