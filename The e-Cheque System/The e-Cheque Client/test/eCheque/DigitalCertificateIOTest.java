/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eCheque;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

/**
 *
 * @author mrivett
 */
public class DigitalCertificateIOTest extends TestCase {
    
    public DigitalCertificateIOTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(DigitalCertificateIOTest.class);
        return suite;
    }

    /**
     * Test of SaveDC method, of class DigitalCertificateIO.
     */
    public void testSaveDC() throws Exception {
        System.out.println("SaveDC");
        DigitalCertificate a = null;
        String filePath = "aasdf.cert";
        DigitalCertificateIO instance = new DigitalCertificateIO();
        instance.SaveDC(a, filePath);
        // TODO review the generated test code and remove the default call to fail.
        instance.SaveDC(a, filePath);
        File f1 = new File(filePath);
        assertTrue(f1.exists() && !f1.isDirectory());
        try {
            filePath = ":test:/df.dslsdF";
            instance.SaveDC(a, filePath);
            File f2 = new File(filePath);
            assertTrue(f2.exists() && !f2.isDirectory());
        } catch (java.io.FileNotFoundException e) {
            assertTrue(true);
        }
        
        System.out.println("readDigitalCertificate");
        // TODO review the generated test code and remove the default call to fail.
        filePath = "aasdf.cert";
        ObjectInputStream In =new ObjectInputStream(new FileInputStream(new File(filePath)));
        DigitalCertificate DC;
        DC =(DigitalCertificate)In.readObject();
        In.close();
        assertTrue(DC == instance.readDigitalCertificate(filePath));
        filePath = ":test:/df.dslsdF";
        try {
            In =new ObjectInputStream(new FileInputStream(new File(filePath)));
            DC =(DigitalCertificate)In.readObject();
            In.close();
            assertTrue(DC == instance.readDigitalCertificate(filePath));
        } catch (java.io.FileNotFoundException e) {
               assertTrue(true);
        }
    }
    
}
