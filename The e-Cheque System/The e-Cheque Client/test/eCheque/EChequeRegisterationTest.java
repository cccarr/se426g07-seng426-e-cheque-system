/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eCheque;

import junit.framework.TestCase;

/**
 *
 * @author brayden
 */
public class EChequeRegisterationTest extends TestCase {
    
    public EChequeRegisterationTest(String testName) {
        super(testName);
    }

    /**
     * Test of setBankName method, of class EChequeRegisteration.
     */
    public void testSetBankName() {
        System.out.println("setBankName");
        String bName = "testname";
        EChequeRegisteration instance = new EChequeRegisteration();
        instance.setBankName(bName);
        String result = instance.getBankName();
        try{
            assertEquals(result, bName);
        } catch (AssertionError e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of setBankAddress method, of class EChequeRegisteration.
     */
    public void testSetBankAddress() {
        System.out.println("setBankAddress");
        String URL = "testaddress";
        EChequeRegisteration instance = new EChequeRegisteration();
        instance.setBankAddress(URL);
        String result = instance.getBankAddress();
        try{
            assertEquals(result, URL);
        } catch (AssertionError e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of setAccountNumber method, of class EChequeRegisteration.
     */
    public void testSetAccountNumber() {
        System.out.println("setAccountNumber");
        String account = "testnumber";
        EChequeRegisteration instance = new EChequeRegisteration();
        instance.setAccountNumber(account);
        String result = instance.getAccountNumber();
        try{
            assertEquals(result, account);
        } catch (AssertionError e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of setClientName method, of class EChequeRegisteration.
     */
    public void testSetClientName() {
        System.out.println("setClientName");
        String cName = "testcname";
        EChequeRegisteration instance = new EChequeRegisteration();
        instance.setClientName(cName);
        String result = instance.getClientName();
        try{
            assertEquals(result, cName);
            return;

        } catch (AssertionError e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of setEWalletLoaction method, of class EChequeRegisteration.
     */
    public void testSetEWalletLoaction() {
        System.out.println("setEWalletLoaction");
        String path = "testlocation";
        EChequeRegisteration instance = new EChequeRegisteration();
        instance.setEWalletLoaction(path);
        String result = instance.getEWalletLoaction();
        try{
            assertEquals(result, path);
            return;
        } catch (AssertionError e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of setUsername method, of class EChequeRegisteration.
     */
    public void testSetUsername() {
        System.out.println("setUsername");
        int hashValue = 1010101;
        EChequeRegisteration instance = new EChequeRegisteration();
        instance.setUsername(hashValue);
        int result = instance.getUsername();
        try{
            assertEquals(result, hashValue);
        } catch (AssertionError e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of setPasword method, of class EChequeRegisteration.
     */
    public void testSetPasword() {
        System.out.println("setPasword");
        int hashValue = 0101010;
        EChequeRegisteration instance = new EChequeRegisteration();
        instance.setPasword(hashValue);
        int result = instance.getPasword();
        try{
            assertEquals(result, hashValue);
        } catch (AssertionError e) {
            fail(e.getMessage());
        }
    }

}
