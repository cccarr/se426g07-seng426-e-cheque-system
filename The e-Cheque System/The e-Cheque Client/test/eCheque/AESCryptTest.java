/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eCheque;

import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mrivett
 */
public class AESCryptTest {
    
    public AESCryptTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of GenerateRandomAESKey method, of class AESCrypt.
     */
    @Test
    public void testGenerateRandomAESKey() throws Exception {
        System.out.println("GenerateRandomAESKey");
        AESCrypt instance = new AESCrypt();
        // TODO review the generated test code and remove the default call to fail.
        assertFalse(instance.GenerateRandomAESKey() == instance.GenerateRandomAESKey());
    }

    /**
     * Test of initializeCipher method, of class AESCrypt.
     */
    @Test
    public void testInitializeCipher() throws Exception {
        AESCrypt instance = new AESCrypt();
        System.out.println("initializeCipher");
        Key key = instance.GenerateRandomAESKey();
        int mode;
        // TODO review the generated test code and remove the default call to fail.
        mode=1;
        int CipherMode=Cipher.DECRYPT_MODE;
        Cipher cipherObj = Cipher.getInstance("AES");
        cipherObj.init(CipherMode,key);
        assertTrue(cipherObj.getAlgorithm() == instance.initializeCipher(key,mode).getAlgorithm());
        /*mode=2;
        CipherMode =Cipher.WRAP_MODE;
        cipherObj = Cipher.getInstance("RSA");
        cipherObj.init(CipherMode,key);
        assertTrue(cipherObj == instance.initializeCipher(key,mode));*/
    }

    /**
     * Test of crypt method, of class AESCrypt.
     */
    @Test
    public void testCrypt() throws Exception {
        System.out.println("crypt");
        InputStream in = null;
        OutputStream out = null;
        Cipher cipherObj = null;
        AESCrypt instance = new AESCrypt();
        
    }

    /**
     * Test of inilizeAESKeyByPassword method, of class AESCrypt.
     */
    @Test
    public void testInilizeAESKeyByPassword() {
        System.out.println("inilizeAESKeyByPassword");
        String pass = "123";
        AESCrypt instance = new AESCrypt();
        // TODO review the generated test code and remove the default call to fail.
        byte[] KeyData = pass.getBytes();
        SecretKeySpec aesKey;
        aesKey =new SecretKeySpec(KeyData,"AES");
        assertTrue(instance.inilizeAESKeyByPassword(pass).equals(aesKey));
    }
    
}