/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eCheque;

import junit.framework.TestCase;
import java.io.File;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;

/**
 *
 * @author cccarr
 */
public class EChequeIOTest extends TestCase {
    
    public EChequeIOTest(String testName) {
        super(testName);
    }

    /**
     * Test of savecheque method, of class EChequeIO.
     */
    public void testSavecheque() throws Exception {
        System.out.println("savecheque");
        ECheque obj = new ECheque();
        String filename = "TestCheque1.txt";
        File filetrue = new File(filename);
        filetrue.createNewFile();
        EChequeIO instance = new EChequeIO();
        instance.savecheque(obj, filename);  
        assertTrue((filetrue.length()>=6));
        //filetrue.delete();
        
        String filenamefalse = "Z:\\TestCheque2.txt";
        File filefalse = new File(filenamefalse);
        instance.savecheque(obj, filenamefalse);
        assertFalse((filefalse.length()!=0));
        
        ECheque obj2 = null;
        String filenametrue2= "TestCheque2.txt";
        File filetrue2 = new File(filenametrue2);
        instance.savecheque(obj2, filenametrue2);  
        assertFalse((filetrue2.length()>=6));
    }

    /**
     * Test of readcheque method, of class EChequeIO.
     */
    public void testReadcheque() throws Exception {
        System.out.println("readcheque");
        String filename = "TestCheque1.txt";
        EChequeIO instance = new EChequeIO();
        ECheque result = instance.readcheque(filename);
        assertNotNull(result);
        //Test if there is no file.
        filename = "TestReadCheque2.txt";
        result = instance.readcheque(filename);
        assertNull(result);
    }
}
