/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eCheque;

import junit.framework.TestCase;

/**
 *
 * @author cccarr
 */
public class EChequeTest extends TestCase {
 
    public EChequeTest(String testName) {
        super(testName);
    }

    /**
     * Test of setaccountholder method, of class ECheque.
     */
    public void testSetaccountholder() {
        System.out.println("setaccountholder");
        String x = "HELLO";
        ECheque instance = new ECheque();
        instance.setaccountholder(x);
        assertEquals(x, instance.getaccountholder());
        
    }

    /**
     * Test of setaccountNumber method, of class ECheque.
     */
    public void testSetaccountNumber() {
        System.out.println("setaccountNumber");
        String x = "HELLO";
        ECheque instance = new ECheque();
        instance.setaccountNumber(x);
        assertEquals(x, instance.getaccountNumber());

    }

    /**
     * Test of setbankname method, of class ECheque.
     */
    public void testSetbankname() {
        System.out.println("setbankname");
        String x = "HELLO";
        ECheque instance = new ECheque();
        instance.setbankname(x);
        assertEquals(x, instance.getbankname());

    }

    /**
     * Test of setpayToOrderOf method, of class ECheque.
     */
    public void testSetpayToOrderOf() {
        System.out.println("setpayToOrderOf");
        String x = "HELLO";
        ECheque instance = new ECheque();
        instance.setpayToOrderOf(x);
        assertEquals(x, instance.getpayToOrderOf());

    }

    /**
     * Test of setamountofMony method, of class ECheque.
     */
    public void testSetamountofMony() {
        System.out.println("setamountofMony");
        String x = "HELLO";
        ECheque instance = new ECheque();
        instance.setamountofMony(x);
        assertEquals(x, instance.getMoney());
    }

    /**
     * Test of setcurrencytype method, of class ECheque.
     */
    public void testSetcurrencytype() {
        System.out.println("setcurrencytype");
        String x = "HELLO";
        ECheque instance = new ECheque();
        instance.setcurrencytype(x);
        assertEquals(x, instance.getcurrencytype());
    }

    /**
     * Test of setchequeNumber method, of class ECheque.
     */
    public void testSetchequeNumber() {
        System.out.println("setchequeNumber");
        String x = "HELLO";
        ECheque instance = new ECheque();
        instance.setchequeNumber(x);
        assertEquals(x, instance.getchequeNumber());
    }

    /**
     * Test of setguaranteed method, of class ECheque.
     */
    public void testSetguaranteed() {
        System.out.println("setguaranteed");
        boolean s = true;
        ECheque instance = new ECheque();
        instance.setguaranteed(s);
        assertEquals(s, instance.getguaranteed());
    }

    /**
     * Test of setearnday method, of class ECheque.
     */
    public void testSetearnday() {
        System.out.println("setearnday");
        String x = "HELLO";
        ECheque instance = new ECheque();
        instance.setearnday(x);
        assertEquals(x, instance.getearnday());
    }

    /**
     * Test of setbanksignature method, of class ECheque.
     */
    public void testSetbanksignature() {
        System.out.println("setbanksignature");
        byte[] x = {(byte)101};
        ECheque instance = new ECheque();
        instance.setbanksignature(x);
        assertEquals(x, instance.getbanksignature());
    }

    /**
     * Test of setdrawersiganure method, of class ECheque.
     */
    public void testSetdrawersiganure() {
        System.out.println("setdrawersiganure");
        byte[] x = {(byte)101};
        //byte[] y = null;
        ECheque instance = new ECheque();
        instance.setdrawersiganure(x);
        assertEquals(x, instance.getdrawersiganure());
    }

    /**
     * Test of getMoney method, of class ECheque.
     */
    public void testGetMoney() {
        System.out.println("getMoney");
        ECheque instance = new ECheque();
        String expResult = "HELLO";
        instance.setamountofMony(expResult);
        String result = instance.getMoney();
        assertEquals(expResult, result);

    }

    /**
     * Test of getaccountholder method, of class ECheque.
     */
    public void testGetaccountholder() {
        System.out.println("getaccountholder");
        ECheque instance = new ECheque();
        String expResult = "HELLO";
        instance.setaccountholder(expResult);
        String result = instance.getaccountholder();
        assertEquals(expResult, result);

    }

    /**
     * Test of getaccountNumber method, of class ECheque.
     */
    public void testGetaccountNumber() {
        System.out.println("getaccountNumber");
        ECheque instance = new ECheque();
        String expResult = "HELLO";
        instance.setaccountNumber(expResult);
        String result = instance.getaccountNumber();
        assertEquals(expResult, result);

    }

    /**
     * Test of getbankname method, of class ECheque.
     */
    public void testGetbankname() {
        System.out.println("getbankname");
        ECheque instance = new ECheque();
        String expResult = "HELLO";
        instance.setbankname(expResult);
        String result = instance.getbankname();
        assertEquals(expResult, result);

    }

    /**
     * Test of getpayToOrderOf method, of class ECheque.
     */
    public void testGetpayToOrderOf() {
        System.out.println("getpayToOrderOf");
        ECheque instance = new ECheque();
        String expResult = "HELLO";
        instance.setpayToOrderOf(expResult);
        String result = instance.getpayToOrderOf();
        assertEquals(expResult, result);

    }

    /**
     * Test of getcurrencytype method, of class ECheque.
     */
    public void testGetcurrencytype() {
        System.out.println("getcurrencytype");
        ECheque instance = new ECheque();
        String expResult = "HELLO";
        instance.setcurrencytype(expResult);
        String result = instance.getcurrencytype();
        assertEquals(expResult, result);

    }

    /**
     * Test of getchequeNumber method, of class ECheque.
     */
    public void testGetchequeNumber() {
        System.out.println("getchequeNumber");
        ECheque instance = new ECheque();
        String expResult = "HELLO";
        instance.setchequeNumber(expResult);
        String result = instance.getchequeNumber();
        assertEquals(expResult, result);

    }

    /**
     * Test of getguaranteed method, of class ECheque.
     */
    public void testGetguaranteed() {
        System.out.println("getguaranteed");
        ECheque instance = new ECheque();
        boolean expResult = true;
        instance.setguaranteed(expResult);
        boolean result = instance.getguaranteed();
        assertEquals(expResult, result);

    }

    /**
     * Test of getearnday method, of class ECheque.
     */
    public void testGetearnday() {
        System.out.println("getearnday");
        ECheque instance = new ECheque();
        String expResult = "HELLO";
        instance.setearnday(expResult);
        String result = instance.getearnday();
        assertEquals(expResult, result);

    }

    /**
     * Test of getbanksignature method, of class ECheque.
     */
    public void testGetbanksignature() {
        System.out.println("getbanksignature");
        ECheque instance = new ECheque();
        byte[] expResult = {(byte)101};
        instance.setbanksignature(expResult);
        byte[] result = instance.getbanksignature();
        assertEquals(expResult, result);

    }

    /**
     * Test of getdrawersiganure method, of class ECheque.
     */
    public void testGetdrawersiganure() {
        System.out.println("getdrawersiganure");
        ECheque instance = new ECheque();
        byte[] expResult = {(byte)101};
        instance.setdrawersiganure(expResult);
        byte[] result = instance.getdrawersiganure();
        assertEquals(expResult, result);

    }
    
}
